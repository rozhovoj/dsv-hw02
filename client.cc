#include <grpcpp/grpcpp.h>
#include <string>
#include <fstream>
#include <streambuf>
#include "histogram.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReaderWriter;

using histogram::StreamMessage;
using histogram::Histogram;

class HistogramClient {
 public:
    HistogramClient(std::shared_ptr<Channel> channel)
      : stub_(Histogram::NewStub(channel)) {}

  // Assembles client payload, sends it to the server, and returns its response
  void sendRequest() {
    // Data to be sent to server
    StreamMessage request;

    // Container for server response
    StreamMessage reply;

    // Context can be used to send meta data to server or modify RPC behaviour
    ClientContext context;

    // stream for messages writing
    std::shared_ptr<ClientReaderWriter<StreamMessage, StreamMessage>> writer (stub_->sendRequest(&context));
    std::string a;
    std::string fileContent;
    int i = 0;
    std::cout << "Give me next file: ";
    do{
        if (i ++ > 0) {
            request.set_msg(fileContent);
            writer->Write(request);
            std::cout << "Give me next file: ";
        }
        std::cin >> a;
        std::ifstream t(a);
        std::string tmp((std::istreambuf_iterator<char>(t)),
                                std::istreambuf_iterator<char>());
        fileContent = tmp;

    }
    while (strcmp(a.c_str(), "STOP") != 0);
    // Returns results based on RPC status
      writer->WritesDone();
      while (writer->Read(&request)) {
          std::cout << "Histogram:\n " << request.msg() << "\n";
      }

      writer->Finish();

    return;
  }

 private:
  std::unique_ptr<Histogram::Stub> stub_;
};

void RunClient() {
  std::string target_address("127.0.0.2:50052");
  // Instantiates the client
  HistogramClient client(
      // Channel from which RPCs are made - endpoint is the target_address
      grpc::CreateChannel(target_address,
                          // Indicate when channel is not authenticated
                          grpc::InsecureChannelCredentials()));

  std::string a = "grpc is cool!";
  std:: cout << a << '\n';

  // RPC is created and response is stored
  client.sendRequest();
}

int main(int argc, char* argv[]) {
  std::cout << "Starting client\n";
  RunClient();

  return 0;
}
