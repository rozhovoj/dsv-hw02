#include <grpcpp/grpcpp.h>
#include <string>

#include <iostream>
#include <sstream>
#include <unordered_map>

#include "histogram.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using grpc::ServerReaderWriter;

using histogram::StreamMessage;
using histogram::Histogram;



// Server Implementation
class HistogramServiceImplementation final : public Histogram::Service {

private:
    
  Status sendRequest(ServerContext* context, ServerReaderWriter<StreamMessage, StreamMessage> * stream) override {
    // Obtains the original string from the request
    std::string word;
    std::string res;

    StreamMessage rq;

    while (stream->Read(&rq))
    {
        std::unordered_map<std::string, int> umap;

        std::istringstream ss(rq.msg());

        while (ss >> word) {

            if (umap.find(word) == umap.end()) {
                umap[word] = 1;
            }
            else {
                umap[word] = umap[word] + 1;
            }
        }
        for (const auto & entry: umap) {
            res.append(entry.first);
            res.append(": ");
            res.append(std::to_string(entry.second));
            res.append(", ");
        }
        StreamMessage reply;
        res.pop_back();

        umap.clear();
        reply.set_msg(res);
        stream->Write(reply);
        res.clear();
    }
            
    return Status::OK;
  }
};

void RunServer() {
  std::string server_address("127.0.0.2:50052");
  HistogramServiceImplementation service;

  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which
  // communication with client takes place
  builder.RegisterService(&service);

  // Assembling the server
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on port: " << server_address << std::endl;

  server->Wait();
}

int main(int argc, char** argv) {
    std::cout << "Starting server\n";
    RunServer();
    return 0;
}